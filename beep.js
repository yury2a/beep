window.AudioContext = window.AudioContext || window.webkitAudioContext;

var context = new AudioContext();

function playSound(buffer, at) {
    var source = context.createBufferSource();
    source.buffer = buffer;
    source.connect(context.destination);
    source.start(context.currentTime + at);
    return source;
}

function copyToChannel(buffer, arr) {
    if (!buffer.copyToChannel) {
        var data = buffer.getChannelData(0);
        data.set(arr);
        return;
    }
    buffer.copyToChannel(arr, 0);    
}

function buildSine(tone, seconds) {
    function sineWaveAt(sampleNumber, tone) {
        var sampleFreq = context.sampleRate / tone
        return Math.sin(sampleNumber / (sampleFreq / (Math.PI*2)))
    }

    var volume = 1;      
    var samplesCount = context.sampleRate * seconds;
    var arr = new Float32Array(samplesCount);
    for (var i = 0; i < samplesCount; i++) {
        arr[i] = sineWaveAt(i, tone) * volume
    }
    var buffer = context.createBuffer(1, samplesCount, context.sampleRate)
    copyToChannel(buffer, arr);
    return buffer;
}

var sine2 = buildSine(1500, 0.2);
var standby = buildSine(440, 0.3);
fetch("standby.wav")
    .then(function (req) { return req.arrayBuffer(); })
    .then(function (buffer) {
        var data = new Uint8Array(buffer).subarray(0x5e);
        var scale = context.sampleRate / 22050;
        var samplesCount = Math.floor(data.length * scale);
        var pcm = new Float32Array(samplesCount);
        for (var i = 0; i < samplesCount; i++) {
            pcm[i] = (data[Math.round(i / scale)] - 0x80) / 0x80;
        }
        standby = context.createBuffer(1, samplesCount, context.sampleRate);
        copyToChannel(standby, pcm);
    })
    .catch(console.error);

function feedback() {
    if (navigator.vibrate) {
        navigator.vibrate([100]);
    }
}

function playSounds(useStandby) {
    var min_start = +$( "#slider_start" ).slider( "values", 0);
    var max_start = +$( "#slider_start" ).slider( "values", 1);
    var start = Math.random() * (max_start - min_start) + min_start;
    var stop = start +  $( "#slider_stop" ).slider( "value");
    var stop2 = stop +  $( "#slider_stop2" ).slider( "value");
    var sources = [];
    if (useStandby) {
        sources.push(playSound(standby, 0));
    }
    sources.push(
        playSound(sine2, start),
        playSound(sine2, stop)
    );
    return {
        stopsIn: useStandby ? stop2 : stop,
        countdown: {
            start: start,
            stop: stop,
        },
        sources: sources
    };
}

var i = 0, playing = null;
function startInterval() {
    feedback();
    $('#startButton').prop("disabled", true);
    $('#playButton').prop("disabled", true);
    if (i) clearInterval(i);
    function tick() {
        var result = playSounds(true);
        var t = Math.ceil(result.stopsIn * 1000);
        i = setTimeout(tick, t);
        playing = result.sources;
    }
    tick();
    $('#stopButton').prop("disabled", false);
}

function stopInterval() {
    feedback();
    $('#stopButton').prop("disabled", true);
    if (playing) {
        playing.forEach(function (source) {
            source.stop();
        });
        playing = null;
    }
    clearTimeout(i);
    i = 0;
    $('#startButton').prop("disabled", false);
    $('#playButton').prop("disabled", false);
}

function playOnce() {
    feedback();
    $('#startButton').prop("disabled", true);
    $('#playButton').prop("disabled", true);
    var t = Math.ceil(playSounds(false).stopsIn * 1000);
    setTimeout(function () {
        $('#playButton').prop("disabled", false);
        $('#startButton').prop("disabled", false);
    }, t);
}

$('#stopButton').prop("disabled", true);
feedback(); // trigger allow popup now